Name: ShiLei Cui. B-Number: B00668965

Project Description:

Background Music and sound effect is implement in SoundToolManager.
Modify the bgMusicType and soundType can change the volume of Sound and Music, method playSoundWithSoundName can play different sound with different name.

Save and load the player record is implement in StageInfoManager.

Loading page animation is implement in LaunchAnimationViewController, In method viewDidAppear() to begin the animation.

Home page is implement in RootViewController.
Set the Plist to different screen size, and get the setting button and start game button frame. In touchesBegan() method do corresponding job.

Select the different stage page is implement in SelectStageViewController, Which will show how many stage the player can play with.

Before the player actually play the game there is a view which will show the game type, game instruction, and the score history. It is implement in PrepareViewController.

The BaseGameViewController will provide the basic attribute for each game, and also initialization.

The RYBViewController will provide the red, yellow, blue button and the background. The Stage 1 and Stage 2 is using this game functionality.

The TwoButtonViewController will provide customized button and customized background image. The Stage 3 and Stage 4 is using this game functionality.

Each Stage end will have a score calculator, which is implement in resultViewController.

Pause game functionality is implement in PauseViewController.