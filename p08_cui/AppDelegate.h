//
//  AppDelegate.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/1/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

