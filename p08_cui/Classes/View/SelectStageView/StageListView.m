//
//  StageListView.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "StageListView.h"
#import "Stage.h"
#import "StageView.h"
#import "StageInfoManager.h"

@implementation StageListView

- (instancetype)init{
    if (self = [super initWithFrame:ScreenBounds]) {
        self.contentSize = CGSizeMake(ScreenWidth, ScreenHeight);
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.pagingEnabled = YES;
        self.delegate = self;
        
        NSArray *bgNames = @[@"select_easy_bg"];
        for (int i = 0; i < 1; i++) {
            FullBackgroundView *listView = [[FullBackgroundView alloc] initWithFrame:CGRectMake(i * ScreenWidth, 0, ScreenWidth, ScreenHeight)];
            [listView setBackgroundImageWihtImageName:bgNames[i]];
            [self addSubview:listView];
        }
        
        [self loadStageInfo];
    }

    return self;
}

- (void)reloadStageForNumber:(int)num {
    StageView *stageView = (StageView *)[self viewWithTag:100 + num - 1];
    Stage *newStage = stageView.stage;
    stageView.stage.userInfo = [[StageInfoManager sharedStageInfoManager] stageInfoWithNumber:num];
    stageView.stage = newStage;
}

- (void)loadStageInfo {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"stages.plist" ofType:nil];
    NSArray *stageArr = [NSArray arrayWithContentsOfFile:path];
    
    CGFloat stageViewW = 120;
    CGFloat stageViewH = 100;
    CGFloat viewMaxgin = ScreenWidth - stageViewW * 2 - 25 * 2;
    CGFloat topMagin = iPhone5 ? 130 : 80;
    StageInfoManager *manager = [StageInfoManager sharedStageInfoManager];
    
    for (int i = 0; i < stageArr.count; i++) {
        Stage *stage = [Stage stageWithDict:stageArr[i]];
        stage.num = i + 1;
        stage.userInfo = [manager stageInfoWithNumber:i + 1];

        CGFloat scrollX = ((int)(i / 6)) * ScreenWidth;
        CGFloat startX = 25 + ((i % 6) / 3) * (stageViewW + viewMaxgin) + scrollX;
        CGFloat startY = topMagin + (i % 3) * (stageViewH + 30);
        
        StageView *stageView = [StageView stageViewWithStage:stage frame:CGRectMake(startX, startY, stageViewW, stageViewH)];
        stageView.tag = 100 + i;
        [self insertSubview:stageView atIndex:5];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stageViewDidSelected:)];
        [stageView addGestureRecognizer:tap];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    if (self.didChangeScrollPage) {
        int page = (scrollView.contentOffset.x / ScreenWidth + 0.5);
        if (page < 0) page = 0;
        if (page > 3) page = 3;
        self.didChangeScrollPage(page);
    }
}

- (void)stageViewDidSelected:(UITapGestureRecognizer *)tap {
    if (self.didSelectedStageView) {
        [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundSelectedStage];
        self.didSelectedStageView(((StageView *)tap.view).stage);
    }
}

@end
