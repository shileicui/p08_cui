//
//  StageListView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Stage;

@interface StageListView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic, copy) void (^didChangeScrollPage)(int page);
@property (nonatomic, copy) void (^didSelectedStageView)(Stage *stage);

- (void)reloadStageForNumber:(int)num;

@end
