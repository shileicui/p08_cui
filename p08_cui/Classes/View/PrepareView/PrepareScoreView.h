//
//  PrepareScoreView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Stage;

@interface PrepareScoreView : UIView

@property (nonatomic, strong) Stage *stage;

- (void)showScroeViewWithCompletion:(void (^)(void))completion;

@end
