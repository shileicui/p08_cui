
//
//  UIColor+Color.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)

+ (instancetype)colorWithR:(int)red g:(int)green b:(int)blue {
    UIColor *color = [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:1];
    return color;
}

+ (instancetype)random {
    return [UIColor colorWithR:arc4random_uniform(256) g:arc4random_uniform(256) b:arc4random_uniform(256)];
}

@end
