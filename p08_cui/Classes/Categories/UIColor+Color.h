//
//  UIColor+Color.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)

+ (instancetype)colorWithR:(int)red g:(int)green b:(int)blue;

+ (instancetype)random;

@end
