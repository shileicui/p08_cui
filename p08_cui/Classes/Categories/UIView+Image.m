//
//  UIView+Image.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "UIView+Image.h"

@implementation UIView (Image)

- (void)setBackgroundImageWihtImageName:(NSString *)imageName {

}

+ (instancetype)viewFromNib {
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil][0];
}

- (void)cleanSawtooth {
    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor clearColor].CGColor;

    self.layer.shouldRasterize = YES;
    
    for (UIView *child in self.subviews) {
        [child cleanSawtooth];
    }
}

@end
