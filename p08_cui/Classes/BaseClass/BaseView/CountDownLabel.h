//
//  CountDownLabel.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "StrokeLabel.h"

@interface CountDownLabel : StrokeLabel

- (instancetype)initWithFrame:(CGRect)frame startTime:(double)time textSize:(CGFloat)size;

- (void)startCountDownWithCompletion:(void (^)(void))completion;
- (void)pause;
- (void)continueWork;
- (void)clean;
- (void)setTextColor:(UIColor *)textColor borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth;;

@end
