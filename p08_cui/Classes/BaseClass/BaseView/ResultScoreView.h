//
//  ResultScoreView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrokeLabel.h"

@protocol ResultScoreViewDelegate <NSObject>

- (void)resultScoreViewChangeWithRank:(NSString *)rank;
- (void)resultScoreViewShowFailViewPassScroeStr:(NSString *)passScroe userScroeStr:(NSString *)userScroe;
- (void)resultScoreViewShowNewCount;
- (void)resultScoreViewDidRemove;

@end

@class Stage;

@interface ResultScoreView : UIView

@property (nonatomic, weak) IBOutlet UIImageView    *hintImageView;
@property (nonatomic, weak) IBOutlet UIImageView    *boardImageView;
@property (nonatomic, weak) IBOutlet StrokeLabel *scroeLabel;
@property (nonatomic, weak) IBOutlet StrokeLabel *unitLabel;
@property (nonatomic, weak) IBOutlet UIView         *labelView;

@property (nonatomic, weak) id <ResultScoreViewDelegate> delegate;

- (void)startCountScoreWithNewScroe:(double)scroe unit:(NSString *)unit stage:(Stage *)stage isAddScore:(BOOL)isAddScroe;

@end
