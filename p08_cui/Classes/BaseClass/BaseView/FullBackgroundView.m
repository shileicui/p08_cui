//
//  FullBackgroundView.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "FullBackgroundView.h"
#import "UIView+Image.h"

@implementation FullBackgroundView
{
    NSString *_bgImageName;
}

- (void)setBackgroundImageWihtImageName:(NSString *)imageName {
    
    _bgImageName = imageName;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
    if (_bgImageName) {
        UIImage *bgImage = [UIImage imageNamed:_bgImageName];
        CGFloat width = 640;
        CGFloat height = iPhone5?1136:960;
        CGRect cutRect = CGRectMake((bgImage.size.width - width)*0.5, (bgImage.size.height - height)*0.5, width, height);
        CGImageRef imageRef = CGImageCreateWithImageInRect(bgImage.CGImage, cutRect);
        bgImage = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        [bgImage drawInRect:rect];
    }

}

@end
