//
//  ScoreboardCountView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrokeLabel.h"
@interface ScoreboardCountView : UIView

@property (weak, nonatomic) IBOutlet StrokeLabel *countLabel;

- (void)startAnimationWithCompletion:(void (^)(BOOL finished))completion;

- (void)hit;

- (void)clean;

@end
