//
//  StateView.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "StateView.h"

@interface StateView ()
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageView;

@property (nonatomic, copy) void (^hiddenFinsih)();
@property (nonatomic, copy) void (^showFinish)();

@end

@implementation StateView

- (void)awakeFromNib {
    self.hidden = YES;
}

- (void)setType:(ResultStateType)type {
    _type = type;
    
    switch (type) {
        case ResultStateTypeGood:
            self.stateImageView.image = [UIImage imageNamed:@"00_good-iphone4"];
            self.circleImageView.image = [UIImage imageNamed:@"00_circle-iphone4"];
            break;
        case ResultStateTypeGreat:
            self.stateImageView.image = [UIImage imageNamed:@"00_great-iphone4"];
            self.circleImageView.image = [UIImage imageNamed:@"00_circle-iphone4"];
            break;
        case ResultStateTypeOK:
            self.stateImageView.image = [UIImage imageNamed:@"00_okay-iphone4"];
            self.circleImageView.image = [UIImage imageNamed:@"00_circle-iphone4"];
            break;
        case ResultStateTypePerfect:
            self.stateImageView.image = [UIImage imageNamed:@"00_perfect-iphone4"];
            self.circleImageView.image = [UIImage imageNamed:@"00_circle-iphone4"];
            break;
        case ResultStateTypeBad:
            self.stateImageView.image = [UIImage imageNamed:@"00_bad-iphone4"];
            self.circleImageView.image = [UIImage imageNamed:@"00_cross-iphone4"];
            break;
        default:
            break;
    }
}

- (void)showStateViewWithType:(ResultStateType)type {
    self.type = type;
    self.hidden = NO;
    switch (type) {
        case ResultStateTypeOK:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundOKName];
            break;
        case ResultStateTypeGood:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundGoodName];
            break;
        case ResultStateTypeGreat:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundGreatName];
            break;
        case ResultStateTypePerfect:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundPerfectName];
        case ResultStateTypeBad:
        {
            NSString *badName = [NSString stringWithFormat:@"instantFail0%d.mp3", arc4random_uniform(3) + 2];
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:badName];
        }
            break;
        default:
            break;
    }
    
    [self insertSubview:self.circleImageView belowSubview:self.stateImageView];
    [self.superview bringSubviewToFront:self];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.hidden = YES;
    });
}

- (void)showStateViewWithType:(ResultStateType)type stageViewHiddenFinishBlock:(void (^)(void))stageViewHiddenFinishBlock {
    self.type = type;
    self.hidden = NO;
    self.hiddenFinsih = stageViewHiddenFinishBlock;
    switch (type) {
        case ResultStateTypeOK:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundOKName];
            break;
        case ResultStateTypeGood:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundGoodName];
            break;
        case ResultStateTypeGreat:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundGreatName];
            break;
        case ResultStateTypePerfect:
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundPerfectName];
            break;
        case ResultStateTypeBad:
        {
            NSString *badName = [NSString stringWithFormat:@"instantFail0%d.mp3", arc4random_uniform(3) + 2];
            [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:badName];
        }
            break;
        default:
            break;
    }
    
    [self insertSubview:self.circleImageView belowSubview:self.stateImageView];
    [self.superview bringSubviewToFront:self];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.hidden = YES;
        if (self.hiddenFinsih) {
            self.hiddenFinsih();
        }
    });
}

- (void)showBadStateWithFinish:(void (^)())finish {
    self.hidden = NO;
    self.stateImageView.layer.anchorPoint = CGPointMake(1, 0.5);
    self.stateImageView.frame = CGRectMake(self.stateImageView.frame.origin.x + self.stateImageView.frame.size.width * 0.5, self.stateImageView.frame.origin.y, self.stateImageView.frame.size.width, self.stateImageView.frame.size.height);
    
    self.showFinish = finish;
    
    NSString *badName = [NSString stringWithFormat:@"instantFail0%d.mp3", arc4random_uniform(3) + 2];
    [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:badName];
    
    self.stateImageView.image = [UIImage imageNamed:@"00_bad-iphone4"];
    self.circleImageView.image = [UIImage imageNamed:@"00_cross-iphone4"];
    
    [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.3 initialSpringVelocity:2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.stateImageView.transform = CGAffineTransformMakeRotation(-M_2_PI);
    } completion:^(BOOL finished) {
        
        self.hidden = YES;
        if (self.showFinish) {
            self.showFinish();
        }
        
    }];
}

- (void)hideStateView {
    self.hidden = YES;
}

- (void)removeData {
    self.showFinish = nil;
    self.hiddenFinsih = nil;
    [self removeFromSuperview];
}

@end
