//
//  StateView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ResultStateType) {
    ResultStateTypeOK = 0,
    ResultStateTypeGood = 1,
    ResultStateTypeGreat = 2,
    ResultStateTypePerfect = 3,
    ResultStateTypeBad = 4
};

@interface StateView : UIView

@property (nonatomic, assign) ResultStateType type;

- (void)showStateViewWithType:(ResultStateType)type;
- (void)hideStateView;

- (void)showStateViewWithType:(ResultStateType)type stageViewHiddenFinishBlock:(void (^)(void))stageViewHiddenFinishBlock;

- (void)showBadStateWithFinish:(void(^)())finish;

- (void)removeData;

@end
