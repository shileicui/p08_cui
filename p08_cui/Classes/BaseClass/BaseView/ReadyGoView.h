//
//  ReadyGoView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadyGoView : UIView

+ (void)showReadyGoViewWithSuperView:(UIView *)superView completion:(void (^)(void))completion;

@end
