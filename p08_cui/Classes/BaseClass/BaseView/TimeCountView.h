//
//  TimeCountView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeCountView : UIView

- (void)startAnimationWithCompletion:(void (^)(BOOL finished))completion;

- (void)startCalculateTime;
- (NSTimeInterval)stopCalculateTime;
- (NSTimeInterval)pasueTime;

- (void)pause;
- (void)resumed;

- (void)cleadData;

@end
