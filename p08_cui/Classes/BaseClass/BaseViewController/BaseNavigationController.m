//
//  BaseNavigationController.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [super pushViewController:viewController animated:NO];
}

@end
