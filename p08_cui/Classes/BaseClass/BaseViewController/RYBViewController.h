//
//  RYBViewController.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "BaseGameViewController.h"

@interface RYBViewController : BaseGameViewController

@property (strong, nonatomic) UIImageView *redImageView;
@property (strong, nonatomic) UIImageView *yellowImageView;
@property (strong, nonatomic) UIImageView *blueImageView;
@property (strong, nonatomic) UIButton    *redButton;
@property (strong, nonatomic) UIButton    *yellowButton;
@property (strong, nonatomic) UIButton    *blueButton;

@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) NSArray *buttonImageNames;

- (void)setButtonsIsActivate:(BOOL)isActivate;


- (void)setButtonImage:(UIImage *)image
      contenEdgeInsets:(UIEdgeInsets)insets;

- (void)removeAllImageView;

- (void)addButtonsActionWithTarget:(id)target
                            action:(SEL)action
                  forControlEvents:(UIControlEvents)forControlEvents;

@end
