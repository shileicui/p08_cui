//
//  BaseGameViewController.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ScoreboardCountView.h"
#import "ResultViewController.h"
#import "StateView.h"

typedef NS_ENUM(NSInteger, GameGuideType) {
    GameGuideTypeNone = 0,
    GameGuideTypeOneFingerClick,
    GameGuideTypeReplaceClick,
    GameGuideTypeMultiPointClick
};

typedef NS_ENUM(NSInteger, ScoreboardType) {
    ScoreboardTypeNone = 0,
    ScoreboardTypeCountPTS,
    ScoreboardTypeTimeMS,
    ScoreboardTypeSecondAndMS,
};

@interface BaseGameViewController : UIViewController

@property (nonatomic, assign) GameGuideType  guideType;
@property (nonatomic, assign) ScoreboardType scoreboardType;
@property (nonatomic, strong) Stage          *stage;

@property (nonatomic, strong) UIView            *countScore;

@property (nonatomic, strong) UIImageView       *guideImageView;
@property (nonatomic, strong) UIButton          *playAgainButton;
@property (nonatomic, strong) UIButton          *pauseButton;

@property (nonatomic, strong) StateView      *stateView;

- (void)beginGame;
- (void)endGame;
- (void)beginRedayGoView;
- (void)readyGoAnimationFinish;
- (void)pauseGame;
- (void)continueGame;
- (void)playAgainGame;
- (void)showGameFail;

- (void)showResultControllerWithNewScroe:(double)scroe
                                    unit:(NSString *)unil
                                   stage:(Stage *)stage
                              isAddScore:(BOOL)isAddScroe;

- (void)buildStageInfo;

- (void)bringPauseAndPlayAgainToFront;

- (void)buildStageView;

@end
