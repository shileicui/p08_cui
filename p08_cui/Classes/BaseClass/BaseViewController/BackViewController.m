//
//  BackViewController.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "BackViewController.h"

@interface BackViewController ()

@end

@implementation BackViewController

- (void)loadView {
    [super loadView];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 50, 48)];
    backButton.adjustsImageWhenHighlighted = NO;
    [backButton setBackgroundImage:[UIImage imageNamed:@"bt_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)backButtonClick {
    [[SoundToolManager sharedSoundToolManager] playSoundWithSoundName:kSoundCliclName];
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

@end
