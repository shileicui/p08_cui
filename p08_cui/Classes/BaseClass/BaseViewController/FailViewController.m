//
//  FailViewController.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "FailViewController.h"
#import "PrepareViewController.h"
#import "StageInfoManager.h"
#import "SelectStageViewController.h"

typedef void(^RetryButtonClickBlock)();

@interface FailViewController ()

@property (nonatomic, copy) RetryButtonClickBlock retryButtonClickBlock;

@end

@implementation FailViewController

+ (instancetype)initWithStage:(Stage *)stage retryButtonClickBlock:(void (^)())retryButtonClickBlock {
    FailViewController *failVC = [[FailViewController alloc] init];
    failVC.stage = stage;
    failVC.retryButtonClickBlock = retryButtonClickBlock;
    
    return failVC;
}

- (IBAction)buttonClick:(UIButton *)sender {
    if (sender.tag == 10) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[SelectStageViewController class]]) {
                [self.navigationController popToViewController:vc animated:NO];
                return;
            }
        }
    } else if (sender.tag == 11) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Lose Reason" message:self.stage.fail delegate:nil cancelButtonTitle:@"Confirm." otherButtonTitles:nil, nil];
        [alertView show];
    }  else if (sender.tag == 13) {
        if (self.retryButtonClickBlock) {
            self.retryButtonClickBlock();
        }
    }
}

@end
