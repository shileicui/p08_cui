//
//  BaseGameViewController.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "BaseGameViewController.h"
#import "StageInfo.h"
#import "ReadyGoView.h"
#import "PauseViewController.h"
#import "CountTimeView.h"
#import "FailView.h"
#import "FailViewController.h"
#import "PrepareViewController.h"
#import "TimeCountView.h"

@interface BaseGameViewController ()
{
    float _volume;
}

@property (nonatomic, strong) UIImageView *adView;

@end

@implementation BaseGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initGuideImageView];
    
    [self buildPlayAgainButton];
    
    [self buildPauseButton];
    
    [self showGuideImageView];
    
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

#pragma mark - Pubilc Method
- (void)playAgainGame {
    self.view.userInteractionEnabled = NO;
    
    [self guideImageViewClick];
}

- (void)pauseGame {
    __weak __typeof(self) weakSelf = self;
    self.view.userInteractionEnabled = NO;
    PauseViewController *pauseVC = [[PauseViewController alloc] init];
    pauseVC.ContinueGameButtonClick = ^ {
        [weakSelf continueGame];
    };
    [self.navigationController pushViewController:pauseVC animated:NO];
}

- (void)buildStageInfo {}

- (void)continueGame {
    self.view.userInteractionEnabled = YES;
}

- (void)readyGoAnimationFinish {
    self.view.userInteractionEnabled = YES;
}

- (void)beginGame {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"kMusicType"] == SoundPlayTypeMute) {
        [[SoundToolManager sharedSoundToolManager] setBackgroundMusicVolume:0.3];
        _volume = 0.3;
    }
}

- (void)endGame {
    if (_volume) {
        [[SoundToolManager sharedSoundToolManager] setBackgroundMusicVolume:1.0];
    }
    
    self.view.userInteractionEnabled = NO;
}

- (void)showGameFail {
    self.view.userInteractionEnabled = NO;
    __weak __typeof(self) weakSelf = self;
    FailView *failView = [FailView viewFromNib];
    failView.frame = CGRectMake(0, ScreenHeight - failView.frame.size.width - 140, failView.frame.size.width, failView.frame.size.height);
    [self.view addSubview:failView];
    [failView showFailViewWithAnimatonFinishBlock:^{
        [weakSelf showFailViewController];
    }];
}

- (void)showFailViewController {
    __weak __typeof(self) weakSelf = self;
    FailViewController *failVC = [FailViewController initWithStage:self.stage retryButtonClickBlock:^{
        for (UIViewController *vc in weakSelf.navigationController.viewControllers) {
            if ([vc isKindOfClass:[PrepareViewController class]]) {
                ((PrepareViewController *)vc).stage = self.stage;
                [weakSelf.navigationController popToViewController:vc animated:NO];
                return;
            }
        }
        
    }];
    [self.navigationController pushViewController:failVC animated:NO];
}

#pragma mark - Private Method
- (void)buildPlayAgainButton {
    self.playAgainButton = [[UIButton alloc] initWithFrame:CGRectMake(ScreenWidth - 55, 75, 110, 52)];
    self.playAgainButton.adjustsImageWhenHighlighted = NO;
    self.playAgainButton.userInteractionEnabled = YES;
    [self.playAgainButton setBackgroundImage:[UIImage imageNamed:@"ing_retry"] forState:UIControlStateNormal];
    [self.playAgainButton addTarget:self action:@selector(playAgainGame) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.playAgainButton];
}

- (void)bringPauseAndPlayAgainToFront {
    [self.view bringSubviewToFront:self.pauseButton];
    [self.view bringSubviewToFront:self.playAgainButton];
    [self.view bringSubviewToFront:self.adView];
    
    if (self.guideImageView) {
        [self.view bringSubviewToFront:self.guideImageView];
    }
}

- (void)buildPauseButton {
    self.pauseButton = [[UIButton alloc] initWithFrame:CGRectMake(ScreenWidth - 55, CGRectGetMaxY(self.playAgainButton.frame) + 13, 110, 52)];
    [self.pauseButton setBackgroundImage:[UIImage imageNamed:@"ing_pause"] forState:UIControlStateNormal];
    self.pauseButton.adjustsImageWhenHighlighted = NO;
    self.pauseButton.userInteractionEnabled = YES;
    [self.pauseButton addTarget:self action:@selector(pauseGame) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.pauseButton];
}

- (void)initGuideImageView {
    self.guideImageView = [[UIImageView alloc] initWithFrame:ScreenBounds];
    [self.view addSubview:self.guideImageView];
}

- (void)showGuideImageView {
    if ((self.stage.userInfo && self.stage.userInfo.rank && ![self.stage.userInfo isEqual:@"f"]) || self.guideType == GameGuideTypeNone) {
        [self guideImageViewClick];
        return;
    }
    
    NSArray *animationImages;
    if (self.guideType == GameGuideTypeOneFingerClick) {
        animationImages = @[[UIImage imageNamed:@"03-1-iphone4"], [UIImage imageNamed:@"03-2-iphone4"]];
    } else if (self.guideType == GameGuideTypeReplaceClick) {
        animationImages = @[[UIImage imageNamed:@"01-1-iphone4"], [UIImage imageNamed:@"01-2-iphone4"]];
    } else if (self.guideType == GameGuideTypeMultiPointClick) {
        animationImages = @[[UIImage imageNamed:@"02-1-iphone4"], [UIImage imageNamed:@"02-2-iphone4"], [UIImage imageNamed:@"02-4-iphone4"], [UIImage imageNamed:@"02-5-iphone4"]];
    }
    
    CGFloat duration;
    if (self.guideType == GameGuideTypeOneFingerClick) {
        duration = 0.3;
    } else if (self.guideType == GameGuideTypeReplaceClick) {
        duration = 0.5;
    } else {
        duration = 0.8;
    }
    self.guideImageView.animationDuration = duration;
    self.guideImageView.animationImages = animationImages;
    self.guideImageView.animationRepeatCount = -1;
    [self.guideImageView startAnimating];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(guideImageViewClick)];
    self.guideImageView.userInteractionEnabled = YES;
    [self.guideImageView addGestureRecognizer:tap];
}

- (void)guideImageViewClick {
    [self.guideImageView removeFromSuperview];
    self.countScore.hidden = NO;
    
    __weak __typeof(self) weakSelf = self;
    if (self.scoreboardType == ScoreboardTypeCountPTS) {
        [(ScoreboardCountView *)self.countScore startAnimationWithCompletion:^(BOOL finished) {
            [weakSelf beginRedayGoView];
        }];
    } else if (self.scoreboardType == ScoreboardTypeTimeMS) {
        [((CountTimeView *)self.countScore) startAnimationWithCompletion:^(BOOL finished) {
            [weakSelf beginRedayGoView];
        }];
    } else if (self.scoreboardType == ScoreboardTypeSecondAndMS) {
        [((TimeCountView *)self.countScore) startAnimationWithCompletion:^(BOOL finished) {
            [weakSelf beginRedayGoView];
        }];
    } else if (self.scoreboardType == ScoreboardTypeNone) {
        [self beginRedayGoView];
    }
}

- (void)buildADView {
    
    
    self.adView.userInteractionEnabled = YES;
    [self.adView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adViewClick)]];
    
    [self.view addSubview:self.adView];
}

- (void)adViewClick {
    
    [self pauseGame];
    
    NSURL *url;

    
    [[UIApplication sharedApplication] openURL:url];
}

- (void)setScoreboardType:(ScoreboardType)scoreboardType {
    _scoreboardType = scoreboardType;
    
    if (scoreboardType == ScoreboardTypeNone) {
        return;
    } else if (scoreboardType == ScoreboardTypeCountPTS) {
        self.countScore = [ScoreboardCountView viewFromNib];
        self.countScore.frame = CGRectMake(-40, -140, self.countScore.frame.size.width, self.countScore.frame.size.height);
    } else if (scoreboardType == ScoreboardTypeTimeMS) {
        self.countScore = [CountTimeView viewFromNib];
        self.countScore.frame = CGRectMake(-60, -140, self.countScore.frame.size.width, self.countScore.frame.size.height);
    } else if (scoreboardType == ScoreboardTypeSecondAndMS) {
        self.countScore = [TimeCountView viewFromNib];
        self.countScore.frame = CGRectMake(-40, -55, self.countScore.frame.size.width, self.countScore.frame.size.height);
    }
    
    self.countScore.hidden = YES;
    if (self.guideImageView) {
        [self.view insertSubview:self.countScore belowSubview:self.guideImageView];
    } else {
        [self.view addSubview:self.countScore];
    }
}

- (void)beginRedayGoView {
    __weak __typeof(self) weakSelf = self;
    self.view.userInteractionEnabled = NO;
    [ReadyGoView showReadyGoViewWithSuperView:self.view completion:^{
        [weakSelf readyGoAnimationFinish];
    }];
}

- (void)showResultControllerWithNewScroe:(double)scroe unit:(NSString *)unil stage:(Stage *)stage isAddScore:(BOOL)isAddScroe {
    ResultViewController *resultVC = [[ResultViewController alloc] init];
    NSLog(@"%f", scroe);
    [resultVC setCountScoreWithNewScroe:scroe unit:unil stage:stage isAddScore:isAddScroe];
    [self.navigationController pushViewController:resultVC animated:NO];
}

- (void)buildStageView {
    self.stateView = [StateView viewFromNib];
    self.stateView.frame = CGRectMake(0, ScreenHeight - self.stateView.frame.size.height - ScreenWidth / 3 - 10, self.stateView.frame.size.width, self.stateView.frame.size.height);
    [self.view addSubview:self.stateView];
}

@end
