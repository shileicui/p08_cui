//
//  ResultViewController.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

- (void)setCountScoreWithNewScroe:(double)scroe unit:(NSString *)unit stage:(Stage *)stage isAddScore:(BOOL)isAddScroe;

@end
