//
//  TwoButtonViewController.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "BaseGameViewController.h"

@interface TwoButtonViewController : BaseGameViewController

@property (nonatomic, strong) UIImageView *backgroundIV;

@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;

- (void)setButtonActivate:(BOOL)isActivate;
@end
