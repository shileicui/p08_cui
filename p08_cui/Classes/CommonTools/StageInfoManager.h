//
//  StageInfoManager.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StageInfo;

@interface StageInfoManager : NSObject

+ (instancetype)sharedStageInfoManager;

- (BOOL)saveStageInfo:(StageInfo *)stageInfo;;
- (StageInfo *)stageInfoWithNumber:(int)number;

@end
