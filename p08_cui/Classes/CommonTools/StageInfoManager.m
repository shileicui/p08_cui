//
//  StageInfoManager.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "StageInfoManager.h"
#import "StageInfo.h"

#define fileName @"stageInfos"
#define path [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:fileName]

@interface StageInfoManager ()

@property (nonatomic, strong) NSMutableDictionary *allStageInfos;

@property (nonatomic, strong) NSArray *aaa;

@end

@implementation StageInfoManager

static StageInfoManager *instance = nil;
+ (instancetype)sharedStageInfoManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [StageInfoManager new];
    });
    
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.allStageInfos = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        
        if (!self.allStageInfos) {
            self.allStageInfos = [NSMutableDictionary dictionary];
            StageInfo *info = [StageInfo new];
            info.num = 1;
            [self.allStageInfos setObject:info forKey:@1];
            
            [self saveStageInfo:info];
        }
    }
    
    return self;
}

- (BOOL)saveStageInfo:(StageInfo *)stageInfo {
    
    if (stageInfo.num <= 0) return NO;
    
    [self.allStageInfos setObject:stageInfo forKey:@(stageInfo.num)];
    
    if (stageInfo.rank && (![stageInfo.rank isEqualToString:@"f"]) && (![self stageInfoWithNumber:stageInfo.num + 1] || ![self stageInfoWithNumber:stageInfo.num + 1].unlock)) {
        StageInfo *nextStageInfo = [[StageInfo alloc] init];
        nextStageInfo.num = stageInfo.num + 1;
        [self saveStageInfo:nextStageInfo];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewStageDidUnLock" object:@(nextStageInfo.num)];
    }

    return [NSKeyedArchiver archiveRootObject:self.allStageInfos toFile:path];
}

- (StageInfo *)stageInfoWithNumber:(int)number {
    NSAssert(number > 0, @"Must greater than 0!");
    
    StageInfo *info = self.allStageInfos[@(number)];
    
    return info;
}

@end
