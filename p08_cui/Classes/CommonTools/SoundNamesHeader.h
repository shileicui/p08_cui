//
//  SoundNamesHeader.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/6/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#define kSoundURLName         @"sound.bundle"
#define kBgMusicURLName       @"bg_music.mp3"

#define kSoundLaunchClickName @"000-click.mp3"
#define kSoundLaunchBoum      @"14_bombExplod.mp3"
#define kSoundLaunchBoum2     @"22_inTheOcean.mp3"

#define kSoundCliclName       @"click.mp3"
#define kSoundUnlock          @"chain_drop.mp3"
#define kSoundNewShow         @"new_pop.mp3"
#define kSoundPrepaerTitle(n) [NSString stringWithFormat:@"drop_title_%d.mp3", n]

#define kSoundReadyGoName     @"ready_go.mp3"
#define kSoundSelectedStage   @"stage_ready.mp3"

#define kSoundNewRecordName1  @"hiScore02.mp3"
#define kSoundNewRecordName2  @"hiScore01.mp3"
#define kSoundSName           @"getGradeS.mp3"
#define kSoundScroeNormalName @"normalScore.mp3"
#define kSoundFailScreamName  @"failShout.mp3"
#define kSoundFailDropName    @"failDrop.mp3"
#define kSoundcageDropName    @"cageDrop.mp3"
#define kSoundOutName         @"instantFail01.mp3"
#define kSoundOutSpoonName    @"whistle.mp3"

//state
#define kSoundOKName      @"okay.mp3"
#define kSoundGoodName   @"good.mp3"
#define kSoundGreatName  @"great.mp3"
#define kSoundPerfectName    @"perfect.mp3"

//stage01
#define kSoundFeatherClickName @"tap.mp3"

//stage02
#define kSoundAppearSoundName @"09_throw.mp3"
#define kSoundRightSoundName @"09_right.mp3"
#define kSoundWrongSoundName @"09_wrong.mp3"

//stage03
#define kSoundWheezeSoundName @"23_drip.mp3"
#define kSOundNoitSoundName   @"23_snore.mp3"

// stage04
#define kSoundScreamName  @"19_scream.mp3"
#define kSoundSlapName @"19_slap.mp3"










