//
//  GameControllerViewManager.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseGameViewController.h"

@interface GameControllerViewManager : NSObject

+ (BaseGameViewController *)viewControllerWithStage:(Stage *)stage;

@end
