//
//  GameControllerViewManager.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "GameControllerViewManager.h"
#import "RYBViewController.h"
#import "Stage01ViewController.h"
#import "Stage02ViewController.h"
#import "Stage03ViewController.h"
#import "Stage04ViewController.h"

@implementation GameControllerViewManager

+ (BaseGameViewController *)viewControllerWithStage:(Stage *)stage {
    BaseGameViewController *gameVC;
    
    switch (stage.num) {
        case 1:
            gameVC = [[Stage01ViewController alloc] init];
            gameVC.stage = stage;
            gameVC.guideType = GameGuideTypeOneFingerClick;
            gameVC.scoreboardType = ScoreboardTypeCountPTS;
            break;
        case 2:
            gameVC = [[Stage02ViewController alloc] init];
            gameVC.stage = stage;
            gameVC.guideType = GameGuideTypeOneFingerClick;
            gameVC.scoreboardType = ScoreboardTypeTimeMS;
            break;
        case 3:
            gameVC = [[Stage03ViewController alloc] init];
            gameVC.stage = stage;
            gameVC.guideType = GameGuideTypeReplaceClick;
            gameVC.scoreboardType = ScoreboardTypeSecondAndMS;
            break;
        case 4:
            gameVC = [[Stage04ViewController alloc] init];
            gameVC.stage = stage;
            gameVC.guideType = ScoreboardTypeCountPTS;
            gameVC.scoreboardType = ScoreboardTypeSecondAndMS;
            break;
        default:
            break;
    }
        
    return gameVC;
}

@end
