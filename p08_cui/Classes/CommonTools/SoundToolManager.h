//
//  SoundToolManager.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/6/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoundNamesHeader.h"

typedef NS_ENUM(NSInteger, SoundPlayType) {
    SoundPlayTypeHight = 0,
    SoundPlayTypeMiddle,
    SoundPlayTypeLow,
    SoundPlayTypeMute
};

@interface SoundToolManager : NSObject

@property (nonatomic, assign) SoundPlayType bgMusicType;
@property (nonatomic, assign) SoundPlayType soundType;

- (void)pauseBgMusic;
- (void)stopBgMusic;
- (void)playBgMusicWihtPlayAgain:(BOOL)playAgain;

- (void)playSoundWithSoundName:(NSString *)soundName;

- (void)setBackgroundMusicVolume:(float)volume;

+ (instancetype)sharedSoundToolManager;

@end
