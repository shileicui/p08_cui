//
//  GuessFingerView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuessFingerView : UIView

@property (nonatomic, copy) void (^animationFinish)(int winIndex);

- (void)startAnimationWithDuration:(NSTimeInterval)duration;

- (void)showResultAnimationCompletion:(void (^)())completion;

- (void)cleanData;

@end
