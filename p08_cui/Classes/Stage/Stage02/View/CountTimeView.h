//
//  CountTimeView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface CountTimeView : UIView

@property (nonatomic, copy) void(^TimeOutBlock)(void);

@property (nonatomic, assign) BOOL notHasTimeOut;

- (void)startAnimationWithCompletion:(void (^)(BOOL))completion;

- (void)startCalculateByTimeWithTimeOut:(void(^)())timeOutBlock outTime:(NSTimeInterval)outTime;
- (void)startCalculateTime;

- (void)stopCalculateByTimeWithTimeBlock:(void(^)(int second, int ms))timeBlock;

- (void)cleanData;

- (void)pause;
- (void)continueGame;

@end
