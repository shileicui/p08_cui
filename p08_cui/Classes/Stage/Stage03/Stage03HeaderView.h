//
//  Stage03HeaderView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Stage03HeaderView : UIView

- (void)startWithFailBlock:(void (^)(void))failBlock stopCalculateTime:(void (^)(void))stopCalculateTime;

- (void)leftBtnClick;

- (void)rightBtnClick;

- (void)pause;
- (void)resumed;

- (void)again;

@end
