//
//  FeatherView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeatherView : UIImageView

- (void)attack:(int)index;

@end
