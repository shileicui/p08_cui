//
//  Stage04PeolpeView.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/10/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Stage04PeolpeView : UIView

@property (nonatomic, copy) void (^failBlock)();

- (void)punchTheFace;

- (void)cleanData;

- (BOOL)doneBtnClick;

@end
