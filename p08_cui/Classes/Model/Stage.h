//
//  Stage.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>

@class StageInfo;

@interface Stage : NSObject

@property (nonatomic, assign) int num;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *format;
@property (nonatomic, copy) NSString *intro;
@property (nonatomic, assign) double max;
@property (nonatomic, assign) double min;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *fail;

@property (nonatomic, strong) StageInfo *userInfo;

+ (instancetype)stageWithDict:(NSDictionary *)dict;

@end
