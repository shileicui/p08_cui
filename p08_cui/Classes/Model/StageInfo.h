//
//  StageInfo.h
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StageInfo : NSObject <NSCoding>

@property (nonatomic, copy) NSString *rank;
@property (nonatomic, assign) int num;
@property (nonatomic, assign) double score;
@property (nonatomic, assign, getter = isUnlock) BOOL unlock;


@end
