//
//  SelectStageViewController.m
//  p08_cui
//
//  Created by SHILEI CUI on 5/9/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "SelectStageViewController.h"
#import "StageListView.h"
#import "PrepareViewController.h"

#define kPrepareIdentifier @"prepare"

@interface SelectStageViewController ()
{
    BOOL _hasNewData;
    int _newNum;
}

@property (nonatomic, strong) StageListView *listView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation SelectStageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundImageWihtImageName:@"select_bg"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newStageDidUnLock:) name:@"NewStageDidUnLock" object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.listView) {
        __weak __typeof(self) weakSelf = self;
        self.listView = [[StageListView alloc] init];
        self.listView.didChangeScrollPage = ^(int page) {
            weakSelf.pageControl.currentPage = page;
        };
        
        self.listView.didSelectedStageView = ^(Stage *stage) {
            [weakSelf performSegueWithIdentifier:kPrepareIdentifier sender:stage];
        };
        
        [self.view insertSubview:self.listView atIndex:0];
    }
    
    if (_hasNewData) {
        [self.listView reloadStageForNumber:_newNum];
        _hasNewData = NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:kPrepareIdentifier]) {
        PrepareViewController *prepaerVC = segue.destinationViewController;
        prepaerVC.stage = (Stage *)sender;
    }
}

- (void)newStageDidUnLock:(NSNotification *)noti {
    _hasNewData = YES;
    _newNum = [noti.object intValue];
}

@end
